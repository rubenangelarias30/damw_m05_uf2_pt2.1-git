import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Data {
 static List<String> courses = Arrays.asList("Python", "Java","Bases de Datos", "FOL","C++");
 static List<String[]> enrollments = new ArrayList<>();
 static List<String> students = Arrays.asList("Eva", "Marc","Guillermina","Júlia","Esteban","Alex","Ruben","Aaron","Clara");
static List<String> teachers = Arrays.asList("Dora", "Sergi","Javier","Daniel","Sergio","Jose Enrique","Israel Aaron","María","Ramiro");
}
